#!/bin/bash
# v 1.0  - script to identify the changes from previous commit and executed the build commands

# constants
head_branch=`sed -e 's#.*/\(\)#\1#' <<< "$CODEBUILD_WEBHOOK_HEAD_REF"` #branch name through which the PR is created
no_action="No changes/updates to config/stack. No action performed by the BUILD"
multiple_changes="BUILD FAILED as multiple stacks requested in same PULL_REQUEST"

#get stack file/action details
get_stack() {
    new_stack=`git diff --name-status $1 $2 | grep /config/`        
    stack_file=`sed -e 's#.*/\(\)#\1#' <<< "$new_stack"`
    stack_name="${stack_file%.*}"
}

#triggered when a PR is created OR updated
function process_PULL_REQUEST (){
    #get the no.of files updated
    new_stack_count=`git diff --name-status master $head_branch | grep /config/ | wc -l`

    if [ $new_stack_count -gt 1 ];then
        echo $multiple_changes
        # Add code to send comment to PR here
        # Add code to auto reject PR here
        exit 1 # sets the build status to failed 
    elif [ $new_stack_count -eq 0 ];then
        echo $no_action
    else              
        get_stack master $head_branch
        sceptre generate-template prod $stack_name
        # Add code to post the generated template to PR here
    fi
}

#triggered when a PR is merged to master
function process_PULL_REQUEST_MERGED (){
    #get the no.of files updated
    new_stack_count=`git diff --name-status HEAD^ HEAD | grep /config/ | wc -l`

    if [ $new_stack_count -gt 1 ];then
        echo $multiple_changes
        exit 1 # sets the build status to failed 
    elif [ $new_stack_count -eq 0 ];then
        echo $no_action
    else
        get_stack HEAD^ HEAD
        stack_action="$(echo $new_stack | head -c 1)"
        case $stack_action in
            A)
                sceptre create-stack prod $stack_name
                ;;
            M)
                sceptre update-stack prod $stack_name
                ;;
            D)
                sceptre delete-stack prod $stack_name
                ;;
            *)
                echo "Currently this operation is not supported"
                ;;
            esac
    fi
}

#main
# get CODEBUILD_WEBHOOK_EVENT (supported types: PULL_REQUEST_UPDATED | PULL_REQUEST_CREATED | PULL_REQUEST_MERGED)
# PUSH is intentionally disabled for now
case $CODEBUILD_WEBHOOK_EVENT in
  PULL_REQUEST_UPDATED | PULL_REQUEST_CREATED)
    process_PULL_REQUEST
    ;;

  PULL_REQUEST_MERGED)
    process_PULL_REQUEST_MERGED
    ;;
  *)
    echo "no action performed by BUILD"
    ;;
esac
# template to create s3 buckets

from awacs.aws import Allow, Statement, Principal, Policy, Action, Condition
from troposphere import Output, Ref, Template, Parameter, Export, Join
from troposphere.s3 import Bucket, PublicRead, BucketPolicy

from tags import TagHelper

class S3Bucket(object):
    def __init__(self, sceptre_user_data):
        self.template = Template()
        self.template.add_description("Create S3 bucket for the customer")
        self.tag_helper = TagHelper(sceptre_user_data)
        self.add_bucket(sceptre_user_data)

    def add_bucket(self, sceptre_user_data):
        bucket_name_param = self.template.add_parameter(Parameter(
            "BucketName",
            Type="String",
            Description="S3 bucket for customer"
        ))

        self.s3_bucket = self.template.add_resource(Bucket(
            "phDataS3Bucket",
            BucketName=Ref(bucket_name_param),
        ))
        self.s3_bucket.Tags = self.tag_helper.get_tags(self.s3_bucket.title)
        self.template.add_output(
            Output(self.s3_bucket.title, Value=(self.s3_bucket.BucketName), Export=Export(self.s3_bucket.BucketName)))

    def get_template(self):
        return self.template


def sceptre_handler(sceptre_user_data):
    s3bucket = S3Bucket(sceptre_user_data)
    return s3bucket.get_template().to_json()

from troposphere.ec2 import Tag as EC2Tag
from troposphere import Tags


class TagHelper(object):

    def __init__(self, sceptre_user_data):
        self.sceptre_tags={tag.split('=')[0]:tag.split('=')[1] for tag in sceptre_user_data["tags"]}
        self.sceptre_tags_list = [EC2Tag(key=tag.split('=')[0], value=tag.split('=')[1]) for tag in sceptre_user_data["tags"]]
        #self.sceptre_tags = Tags(**{tag.split('=')[0]:tag.split('=')[1] for tag in sceptre_user_data["tags"]})

    def get_tags_list(self):
        return self.sceptre_tags_list

    def get_tags(self, name):
        rtn = self.sceptre_tags.copy()
        rtn["Name"] = name
        return Tags(**rtn)